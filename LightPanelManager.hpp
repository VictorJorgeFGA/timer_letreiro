#ifndef _LIGHT_PANEL_MANAGER_HPP_
#define _LIGHT_PANEL_MANAGER_HPP_

#include "RTClib.h"

#define TEMPO_DE_DESCANSO 60000

const uint8_t RELAY = 12;
const uint16_t _17h_45min = 1065;
const uint16_t _21h = 1260;
const uint16_t _20h = 1200;

class LightPanelManager
{
    public:

        LightPanelManager(){};
        ~LightPanelManager(){};

        static bool eHoraDeLigarAsLuzes();
        static void ligarLuzes();
        static void desligarLuzes();
        static void esperar();

};

#endif
