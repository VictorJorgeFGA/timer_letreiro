#include "LightPanelManager.hpp"
#include "RTClib.h"

bool LightPanelManager::eHoraDeLigarAsLuzes()
{
    const DateTime & currentDateTime = RTC_DS1307::now();
    bool resultFlag = false;

    int16_t totalDayElapsedTime = currentDateTime.hour() * 60
                                + currentDateTime.minute();

    switch( currentDateTime.dayOfTheWeek() )
    {
        case 0:
        case 6:
            if( totalDayElapsedTime >= _17h_45min and totalDayElapsedTime <= _20h )
            {
                resultFlag = true;
            }
            break;
        
        default:
            if( totalDayElapsedTime >= _17h_45min and totalDayElapsedTime <= _21h )
            {
                resultFlag = true;
            }
            break;
    }

    return resultFlag;
}

void LightPanelManager::ligarLuzes()
{
    digitalWrite( RELAY , LOW );
}

void LightPanelManager::desligarLuzes()
{
    digitalWrite( RELAY , HIGH );
}

void LightPanelManager::esperar()
{
    delay( TEMPO_DE_DESCANSO );
}