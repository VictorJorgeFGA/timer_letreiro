#include "LightPanelManager.hpp"

RTC_DS1307 rtc;
LightPanelManager letreiro;

void setup() {
    pinMode(RELAY, OUTPUT);

  Serial.begin(57600);
  if( not rtc.begin() )
  {
    Serial.println("RTC nao encontrado!");
    while( true ) {}
  }

  if( not rtc.isrunning() )
  {
    Serial.println("RTC esta em um estado inconsistente");
    rtc.adjust( DateTime( 2019, 8, 2, 18, 49, 0 ));
  }
}

void loop() {
    // DateTime a = rtc.now();
    // Serial.println( a.day() );
    // Serial.println( a.month() );
    // Serial.println( a.year() );
    // Serial.println( a.hour() );
    // Serial.println( a.minute() );
    // Serial.println( a.second() );
    // Serial.println();
    
    if( letreiro.eHoraDeLigarAsLuzes() )
    {
        letreiro.ligarLuzes();
    }
    else
    {
        letreiro.desligarLuzes();
    }
    
    letreiro.esperar();
}
